﻿using lab1_zad1_zegar.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1_zad1_zegar
{
    public partial class Form1 : Form
    {
        Alarm clockAlarm = new Alarm();

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // added a new comment
            int alarmMinutes;
            int timeMinutes;

            label1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            label2.Text = DateTime.Now.ToString("HH:mm:ss");

            alarmMinutes = clockAlarm.minutes + clockAlarm.hours * 60;
            timeMinutes = DateTime.Now.Hour*60 + DateTime.Now.Minute;

            if (alarmMinutes == timeMinutes && clockAlarm.isSet)
            {
                clockAlarm.isSet = false;
                MessageBox.Show(DateTime.Now.ToString(), "ALARM!");
            }

            if (timeMinutes.Equals(0))
            {
                clockAlarm.isSet = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(DateTime.Now.Hour.ToString());
            clockAlarm.hours = (int)aHours.Value;
            clockAlarm.minutes = (int)aMinutes.Value;
            clockAlarm.isSet = true;
            label6.Text = clockAlarm.hours.ToString() + ":" + clockAlarm.minutes.ToString();
        }

 
    }
}
