﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1_zad1_zegar.Model
{
    class Alarm
    {
        public int minutes;
        public int hours;
        public bool isSet;

        public Alarm ()
        {
            minutes = 0;
            hours = 0;
            isSet = false;
        }
    }
}
